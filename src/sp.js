const express = require('express');
const log4js = require('log4js');
const path = require('path');
const session = require('express-session');
// const FileStore = require('session-file-store')(session);
const jwt = require('jsonwebtoken');
const http = require('http');

log4js.configure({
    appenders: { 'out': { type: 'stdout' } },
    categories: { default: { appenders: ['out'], level: 'trace' } }
    // appenders: { serverlog: { type: 'file', filename: 'serverlog.log' } },
    // categories: { default: { appenders: ['serverlog'], level: 'error' } }
});

const logger = log4js.getLogger('sp');

// logger.trace('trace');
// logger.debug('debug');
// logger.info('info');
// logger.warn('warn');
// logger.error('error');
// logger.fatal('fatal');

const app = express();

app.use(log4js.connectLogger(logger, { level: 'info' }));

app.set('trust proxy', 1) // trust first proxy
app.use(session({
    // store: new FileStore({}),
    name : 'connect.sid.sp', // needed for demo where IdP and SP are on localhost
    secret: 'session secret sp',
    resave: true,
    saveUninitialized: true,
    cookie: {
        // secure: true
    }
}));

const jwtIdpSecretOrPublicKey = 'idp secret';
const jwtSpSecretOrPrivateKey = 'sp secret';
const jwtExp = 1;

function spAuth(req, res, next) {
    if (req.query.jwt) {
        try {
            const jwtIdpData = jwt.verify(req.query.jwt, jwtIdpSecretOrPublicKey);
            logger.trace('SP JWT verified', jwtIdpData);
            req.session.user = jwtIdpData;
            next();
        } catch (err) {
            logger.error('SP JWT verify error', err);
            res.send(401, err.message);
        }
    } else if (req.session.user) {
        logger.warn('SP Session JWT', req.session.user);
        next();
    } else {
        const spJwtData = {
            sub: 'peter',
            iss: 'sp',
            aud: 'idp',
            sp: req.protocol + '://' + req.get('host') + '/sp',
            url: req.originalUrl,
            iat: Math.floor(Date.now() / 1000),
            exp: Math.floor(Date.now() / 1000) + jwtExp
        };
        const spJwt = jwt.sign(
            spJwtData,
            jwtSpSecretOrPrivateKey);
        if (req.xhr) {
            res.sendStatus(401);
        } else {
            logger.trace('SP redirect to IdP', `http://localhost:3000/idp/auth?jwt=${spJwt}`, spJwtData);
            res.redirect(`http://localhost:3000/idp/auth?jwt=${spJwt}`);
        }
    }
}

app.get('/', (req, res) => {
    res.contentType('text/html');
    res.send(`<h1>SP</h1>
        <p>Logged: <b>${JSON.stringify(req.session.user)}</b></p>
        <a href="protected">Protected page (requires login)</a><br/>
        <a href="logout">Logout</a>`);
});

app.get('/protected', spAuth, (req, res) => {
    res.send(`SP Session JWT
        <b>${JSON.stringify(req.session.user)}</b><br/>
        <a href="/">Home</a>`);
});

app.get('/protected.json', spAuth, (req, res) => {
    res.json(req.session.user);
});

app.get('/logout', (req, res) => {
    req.session.destroy();
    res.redirect("/");
});

app.get('/sp', (req, res) => {
    if (req.query.jwt) {
        jwtRenewTest(req.query.jwt, 100);
        try {
            const jwtIdpData = jwt.verify(req.query.jwt, jwtIdpSecretOrPublicKey);
            logger.trace('SP JWT verified', jwtIdpData);
            req.session.user = jwtIdpData;
            res.redirect(jwtIdpData.url);
        } catch (err) {
            logger.error('SP JWT verify error', err);
            res.send(401, err.message);
        }
    }
});

function jwtRenewTest(idpJwt, exp = 60) {
    http
        .request(
            {
                host: 'localhost',
                port: 3000,
                path: `/idp/renew?jwt=${idpJwt}&exp=${exp}`
            },
            response => {
                var data = '';
                response.on('error', err => {
                    logger.error('SP renew JWT error', response.statusCode, err);
                });
                response.on('data', chunk => {
                    data += chunk;
                });
                response.on('end', () => {
                    try {
                        logger.trace('SP renew JWT', response.statusCode, data);
                        const jwtIdpData = jwt.verify(data, jwtIdpSecretOrPublicKey);
                        logger.trace('SP renew JWT verified', jwtIdpData);
                    } catch (err) {
                        logger.error('SP JWT verify error', err);
                    }
                });
            })
        .end();
}

app.use(express.static(path.join(__dirname, '..', 'public')));

const port = 3001;
app.listen(port, () => console.log(`Listening at http://localhost:${port}`));
