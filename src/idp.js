const express = require('express');
const log4js = require('log4js');
const path = require('path');
const session = require('express-session');
// const FileStore = require('session-file-store')(session);
const jwt = require('jsonwebtoken');
const bodyParser = require('body-parser');

log4js.configure({
    appenders: { 'out': { type: 'stdout' } },
    categories: { default: { appenders: ['out'], level: 'trace' } }
    // appenders: { serverlog: { type: 'file', filename: 'serverlog.log' } },
    // categories: { default: { appenders: ['serverlog'], level: 'error' } }
});

const logger = log4js.getLogger('idp');

// logger.trace('trace');
// logger.debug('debug');
// logger.info('info');
// logger.warn('warn');
// logger.error('error');
// logger.fatal('fatal');

const app = express();

app.use(log4js.connectLogger(logger, { level: 'info' }));

app.set('trust proxy', 1) // trust first proxy
app.use(session({
    // store: new FileStore({}),
    name : 'connect.sid.idp', // needed for demo where IdP and SP are on localhost
    secret: 'session secret idp',
    resave: true,
    saveUninitialized: true,
    cookie: {
        // secure: true
    }
}));

app.use(bodyParser.urlencoded({ extended: false }));

const jwtIdpSecretOrPrivateKey = 'idp secret';
const jwtIdpSecretOrPublicKey = 'idp secret';
const jwtExp = 100;

const jwtSpSecretOrPublicKeys = {
    sp: 'sp secret',
    app: 'app secret'
}

const credentials = {
    // login: password
    peter: 'heslo',
    martin: 'heslo'
}

const roles = {
    // login: {
    //      app: [role, ...]
    // }
    peter: {
        sp: ['role1', 'role2']
    },
    martin: {
        sp: ['role2', 'role3']
    }
};

app.get('/', (req, res) => {
    res.contentType('text/html');
    res.send(`<h1>IdP</h1>
        <p>User: <b>${req.session.user || ''}</b></p>
        <a href="login">Login</a><br/>
        <a href="logout">Logout</a>`);
});

app.all('/login', (req, res) => {
    let errMessage = '';
    if (req.method === "POST") {
        const pwd = credentials[req.body.username];
        logger.info("Login:", JSON.stringify(req.body));
        if (req.body.username && pwd && req.body.password === pwd) {
            req.session.user = req.body.username;
            if (req.session.spJwtData) {
                const spRedirectData = spJwtRedirectData(req.session.spJwtData, req.session.user);
                delete req.session.spJwtData;
                logger.trace('IdP redirect to SP', spRedirectData.spRedirectUrl, spRedirectData.idpJwtData);
                res.redirect(spRedirectData.spRedirectUrl);
            } else {
                res.redirect("/");
            }
            return;
        } else {
            errMessage = 'Invalid credentials';
        }
    }
    res.contentType('text/html');
    res.send(`
        <h1>IdP Login</h1>
        <p>Logged: ${req.session.user || ''}</p>
        <form method="post">
            <input type="text" name="username" value="${req.session.spJwtData && req.session.spJwtData.sub || ''}"/>
            <input type="text" name="password" value=""/>
            <input type="submit" value="Submit"/>
        </form>
        <p style="color: red;">${errMessage}</p>`);
});

app.get('/logout', (req, res) => {
    req.session.destroy();
    res.redirect("/");
});

app.get('/idp/auth', (req, res) => {
    if (req.query.jwt) {
        const jwtData = jwt.decode(req.query.jwt);
        logger.trace('IdP JWT data', jwtData);
        try {
            const spJwtData = jwt.verify(req.query.jwt, jwtSpSecretOrPublicKeys[jwtData.iss]);
            logger.trace('IdP JWT verified', spJwtData);
            logger.trace('IdP Session', JSON.stringify(req.session));
            if (req.session.user) {
                const sub = req.session.user;
                const spRedirectData = spJwtRedirectData(spJwtData, sub);
                logger.trace('IdP redirect to SP', spRedirectData.spRedirectUrl, spRedirectData.idpJwtData);
                res.redirect(spRedirectData.spRedirectUrl);
            } else {
                // AUTH User spJwtData.sub or from form, create session
                logger.warn('IdP AUTH', spJwtData.sub);
                req.session.spJwtData = spJwtData;
                logger.trace('IdP Session', JSON.stringify(req.session));
                res.redirect('/login');
            }
        } catch (err) {
            logger.error('IdP JWT verify error', err);
            res.send(401, err.message);
        }
    }  else {
        logger.trace('Error: No request JWT');
        res.send('Error: No request JWT');
    }
});

app.get('/idp/renew', (req, res) => {
    if (req.query.jwt) {
        try {
            const idpJwtData = jwt.verify(req.query.jwt, jwtIdpSecretOrPublicKey);
            logger.trace('IdP renew JWT verified', idpJwtData);
            const newIdpJwtData = {
                ...idpJwtData,
                url: undefined,
                ren: idpJwtData.ren ? ++idpJwtData.ren : 1,
                iat: Math.floor(Date.now() / 1000),
                exp: Math.floor(Date.now() / 1000) + (Number(req.query.exp) || 60)
            };
            const newIdpJwt = jwt.sign(newIdpJwtData, jwtIdpSecretOrPrivateKey);
            logger.trace('IdP JWT renewed', newIdpJwt, newIdpJwtData);
            res.send(newIdpJwt);
        } catch (err) {
            logger.error('IdP renew JWT verify error', err);
            res.send(401, err.message);
        }
    }  else {
        logger.trace('Error: No renew request JWT');
        res.send('Error: No renew request JWT');
    }
});

function spJwtRedirectData(spJwtData, sub) {
    const idpJwtData = {
        sub: sub,
        roles: roles[sub][spJwtData.iss],
        iss: 'idp',
        aud: spJwtData.iss,
        url: spJwtData.url
    };
    const idpJwt = jwt.sign(
        {
            ...idpJwtData,
            ren: 0, // renew counter
            iat: Math.floor(Date.now() / 1000),
            exp: Math.floor(Date.now() / 1000) + jwtExp
        },
        jwtIdpSecretOrPrivateKey);
    return {
        idpJwtData,
        idpJwt,
        spRedirectUrl: `${spJwtData.sp}?jwt=${idpJwt}`
    };
}

app.use(express.static(path.join(__dirname, '..', 'public')));

const port = 3000;
app.listen(port, () => console.log(`Listening at http://localhost:${port}`));
